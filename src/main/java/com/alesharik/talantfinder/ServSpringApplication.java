package com.alesharik.talantfinder;

import com.alesharik.talantfinder.extension.IntegrationBean;
import com.alesharik.talantfinder.extension.IntegrationManager;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;

import java.util.Map;

@SpringBootApplication
@Slf4j
public class ServSpringApplication {

    public static void main(String[] args) {
        ConfigurableApplicationContext context = SpringApplication.run(ServSpringApplication.class, args);
        IntegrationManager.scanForBeans(context);
        IntegrationManager.start();
    }

    @Bean
    public static DisposableBean getShutdownHook() {
        return IntegrationManager::stop;
    }
}
