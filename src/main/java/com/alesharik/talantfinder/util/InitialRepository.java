package com.alesharik.talantfinder.util;

import com.alesharik.talantfinder.database.entity.*;
import com.alesharik.talantfinder.database.repository.*;
import com.github.javafaker.Faker;
import lombok.experimental.UtilityClass;

import java.util.*;
import java.util.stream.Collectors;

@UtilityClass
public class InitialRepository {
    public static boolean isNotInitialized(HumanRepository repository) {
        return repository.findAllByRole(Human.Role.ADMIN).isEmpty();
    }

    public static void initRepository(HumanRepository repository) {
        repository.createUser(Human.Role.ADMIN, "admin", "admin", "admin");
        repository.createUser(Human.Role.USER, "Abdul Ahmid", "abdul", "boom");
    }

    public static void initRandomData(AbilityTypeRepository abilityTypeRepository, AbilityRepository abilityRepository, ContactsRepository contactsRepository, HumanRepository humanRepository, NoteRepository noteRepository, StatsRepository statsRepository) {
        List<Human> admins = Arrays.asList(
                humanRepository.createUser(Human.Role.ADMIN, "admin0", "admin0", "admin0"),
                humanRepository.createUser(Human.Role.ADMIN, "admin1", "admin1", "admin1"),
                humanRepository.createUser(Human.Role.ADMIN, "admin2", "admin2", "admin2"),
                humanRepository.createUser(Human.Role.ADMIN, "admin3", "admin3", "admin3"));
        Random random = new Random();
        Faker faker = new Faker();
        AbilityType progLang = new AbilityType("programming language", "#00FF00");
        AbilityType tech = new AbilityType("tech", "#00FFFF");
        progLang = abilityTypeRepository.save(progLang);
        tech = abilityTypeRepository.save(tech);
        List<Ability> abilities = Arrays.asList(new Ability("React", tech), new Ability("Angular", tech), new Ability("Vue.js", tech),
                new Ability("Java", progLang), new Ability("JavaScript", progLang), new Ability("TypeScript", progLang), new Ability("Pascal.ABC", progLang),
                new Ability("Unity", tech), new Ability("UE4", tech));
        abilities = abilities.stream()
                .map(abilityRepository::save)
                .collect(Collectors.toList());
        for (int i = 0; i < 1000; i++) {
            Human human = new Human(random.nextBoolean() ? Human.Role.USER : Human.Role.HUMAN_RESOURCE, faker.name().name(), faker.name().username(), "root");
            Contacts phone = new Contacts("Phone", faker.phoneNumber().cellPhone());
            Contacts email = new Contacts("Email", faker.internet().safeEmailAddress());
            phone = contactsRepository.save(phone);
            email = contactsRepository.save(email);
            human.setContacts(new HashSet<>());
            human.setAbilities(new HashSet<>());
            human.setStats(new HashSet<>());
            human.getContacts().add(phone);
            human.getContacts().add(email);
            for (Ability ability : abilities) {
                if(random.nextBoolean())
                    human.getAbilities().add(ability);
            }
            for (Ability ability : human.getAbilities()) {
                if(random.nextBoolean()) {
                    List<Note> notes = new ArrayList<>();
                    for (Human admin : admins) {
                        if(random.nextBoolean()) {
                            Note note = new Note(Note.NoteValue.fromInt(random.nextInt(4) + 1), admin);
                            note = noteRepository.save(note);
                            notes.add(note);
                        }
                    }
                    Stats stats = new Stats(ability, random.nextInt(2500), new HashSet<>(notes));
                    stats = statsRepository.save(stats);
                    human.getStats().add(stats);
                }
            }
            humanRepository.save(human);
        }
    }
}
