package com.alesharik.talantfinder.rest;

import com.alesharik.talantfinder.database.repository.AbilityRepository;
import com.alesharik.talantfinder.database.repository.AbilityTypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/global")
public class GlobalDataController {
    @Autowired
    private AbilityTypeRepository abilityTypeRepository;
    @Autowired
    private AbilityRepository abilityRepository;

    @GetMapping("/types")
    public ResponseEntity<Object> getTypes() {
        return new ResponseEntity<>(abilityTypeRepository.findAll(), HttpStatus.OK);
    }

    @GetMapping("/abilities")
    public ResponseEntity<Object> getAbilities() {
        return new ResponseEntity<>(abilityRepository.findAll(), HttpStatus.OK);
    }
}
