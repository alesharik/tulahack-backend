package com.alesharik.talantfinder.rest;

import com.alesharik.talantfinder.database.entity.Ability;
import com.alesharik.talantfinder.database.entity.Human;
import com.alesharik.talantfinder.database.entity.Stats;
import com.alesharik.talantfinder.database.repository.HumanRepository;
import com.alesharik.talantfinder.extension.IntegrationBean;
import com.alesharik.talantfinder.extension.IntegrationManager;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;

@RestController
public class SearchController {
    private static final Gson GSON = new GsonBuilder()
            .create();

    @Autowired
    private HumanRepository repository;

    @PostMapping("/api/search")
    public ResponseEntity<Object> search(@RequestBody String body) {
        SearchQuery query = GSON.fromJson(body, SearchQuery.class);
        List<Human> ret = new ArrayList<>();
        main:
        for (Human human : repository.findAll()) {
            for (SearchAbility ability : query.getAbilities()) {
                Ability abil = null;
                for (Ability humanAbility : human.getAbilities()) {
                    if(humanAbility.getName().equals(ability.ability)) {
                        abil = humanAbility;
                        break;
                    }
                }
                if(abil == null)
                    continue main;
                for (Stats stat : human.getStats()) {
                    boolean has = false;
                    if(stat.getAbility().getId() == abil.getId()) {
                        has = true;
                        if(ability.hours != -1 && stat.getHours() < ability.hours)
                            continue main;
                        int average = (int) stat.getNotes().stream()
                                .mapToInt(note -> note.getNote().toInt())
                                .collect(IntSummaryStatistics::new, IntSummaryStatistics::accept, IntSummaryStatistics::combine)
                                .getAverage();
                        if(ability.note != -1 && average < ability.note)
                            continue main;
                    }
                    if(!has)
                        continue main;
                }
            }
            for (IntegrationBean bean : IntegrationManager.getBeans()) {
                if(!bean.getSearchQueryManager().isValid(query.extensions, human))
                    continue main;
            }
            ret.add(human);
        }
        return new ResponseEntity<>(ret, HttpStatus.OK);
    }

    @Data
    private static final class SearchQuery {
        private final List<SearchAbility> abilities = new ArrayList<>();
        private final Map<String, String> extensions = new HashMap<>();
    }

    @Data
    private static final class SearchAbility {
        private final String ability;
        private final int hours;
        private final int note;
    }
}
