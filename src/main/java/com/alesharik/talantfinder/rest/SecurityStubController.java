package com.alesharik.talantfinder.rest;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SecurityStubController {
    @GetMapping("/success")
    public String success() {
        return "success";
    }
}
