package com.alesharik.talantfinder.rest;

import com.alesharik.talantfinder.database.entity.Human;
import com.alesharik.talantfinder.database.entity.Integration;
import com.alesharik.talantfinder.database.entity.IntegrationField;
import com.alesharik.talantfinder.database.repository.HumanRepository;
import com.alesharik.talantfinder.database.repository.IntegrationFieldRepository;
import com.alesharik.talantfinder.database.repository.IntegrationRepository;
import com.alesharik.talantfinder.rest.exception.NotFoundException;
import com.alesharik.talantfinder.rest.security.UserDetailsImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/user")
public class IntegrationController {
    @Autowired
    private HumanRepository humanRepository;
    @Autowired
    private IntegrationRepository integrationRepository;
    @Autowired
    private IntegrationFieldRepository integrationFieldRepository;

    @GetMapping("/integrations")
    public ResponseEntity<Object> get(@RequestParam("uid") long uid) {
        Human human = ((UserDetailsImpl) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getHuman();
        for (Integration integration : human.getIntegrations())
            if (integration.getId() == uid)
                return new ResponseEntity<>(integration.getFields(), HttpStatus.OK);
        throw new NotFoundException();
    }

    @PutMapping("/integration")
    public ResponseEntity<Object> patch(@RequestParam("uid") long uid, @RequestParam("name") String name, @RequestParam("value") String value) {
        Human human = ((UserDetailsImpl) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getHuman();
        for (Integration integration : human.getIntegrations()) {
            if(integration.getId() == uid) {
                IntegrationField field = new IntegrationField(name, value);
                IntegrationField f = integrationFieldRepository.save(field);
                integration.getFields().add(f);
                integrationRepository.save(integration);
                return new ResponseEntity<>(f, HttpStatus.OK);
            }
        }
        throw new NotFoundException();
    }

    @DeleteMapping("/integration")
    public ResponseEntity<Object> delete(@RequestParam("uid") long uid, @RequestParam("name") String name) {
        Human human = ((UserDetailsImpl) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getHuman();
        for (Integration integration : human.getIntegrations()) {
            if(integration.getId() == uid) {
                for (IntegrationField field : integration.getFields()) {
                    if(field.getKey().equals(name)) {
                        integration.getFields().remove(field);
                        integrationRepository.save(integration);
                        integrationFieldRepository.delete(field);
                        return new ResponseEntity<>(field, HttpStatus.OK);
                    }
                }
            }
        }
        throw new NotFoundException();

    }
}
