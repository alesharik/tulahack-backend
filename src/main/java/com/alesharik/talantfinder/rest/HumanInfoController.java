package com.alesharik.talantfinder.rest;

import com.alesharik.talantfinder.database.entity.Human;
import com.alesharik.talantfinder.database.repository.HumanRepository;
import com.alesharik.talantfinder.rest.exception.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/admin")
public class HumanInfoController {
    @Autowired
    private HumanRepository repository;

    @GetMapping("/user/info")
    public ResponseEntity<Object> getUserInfo(@RequestParam("id") long id) {
        Human human = repository.findById(id).orElseThrow(NotFoundException::new);
        return new ResponseEntity<>(human, HttpStatus.OK);
    }
}
