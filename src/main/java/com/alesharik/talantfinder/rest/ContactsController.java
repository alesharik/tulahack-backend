package com.alesharik.talantfinder.rest;

import com.alesharik.talantfinder.database.entity.Contacts;
import com.alesharik.talantfinder.database.entity.Human;
import com.alesharik.talantfinder.database.repository.ContactsRepository;
import com.alesharik.talantfinder.database.repository.HumanRepository;
import com.alesharik.talantfinder.rest.exception.NotFoundException;
import com.alesharik.talantfinder.rest.security.UserDetailsImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

@RestController("/user")
public class ContactsController {
    @Autowired
    private ContactsRepository contactsRepository;
    @Autowired
    private HumanRepository humanRepository;

    @GetMapping("/contacts")
    public ResponseEntity<Object> getContacts() {
        Human human = ((UserDetailsImpl) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getHuman();
        return new ResponseEntity<>(human.getContacts(), HttpStatus.OK);
    }

    @PutMapping("/contacts")
    public ResponseEntity<Object> put(@RequestParam("service") String service, @RequestParam("address") String address) {
        Human human = ((UserDetailsImpl) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getHuman();
        Contacts contacts = new Contacts();
        contacts.setAddress(address);
        contacts.setService(service);
        Contacts c = contactsRepository.save(contacts);
        human.getContacts().add(c);
        humanRepository.save(human);
        return new ResponseEntity<>(contacts, HttpStatus.OK);
    }

    @PatchMapping("/contacts")
    public ResponseEntity<Object> patch(@RequestParam("id") long id, @RequestParam("service") String service, @RequestParam("address") String address) {
        Human human = ((UserDetailsImpl) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getHuman();
        Contacts contacts = contactsRepository.findById(id).orElseThrow(NotFoundException::new);
        if(!human.getContacts().contains(contacts))
            return new ResponseEntity<>(null, HttpStatus.FORBIDDEN);
        contacts.setService(service);
        contacts.setAddress(address);
        Contacts save = contactsRepository.save(contacts);
        return new ResponseEntity<>(save, HttpStatus.OK);
    }

    @DeleteMapping("/contacts")
    public ResponseEntity<Object> delete(@RequestParam("id") long id) {
        Human human = ((UserDetailsImpl) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getHuman();
        Contacts contacts = contactsRepository.findById(id).orElseThrow(NotFoundException::new);
        if(!human.getContacts().contains(contacts))
            return new ResponseEntity<>(null, HttpStatus.FORBIDDEN);
        human.getContacts().remove(contacts);
        humanRepository.save(human);
        contactsRepository.delete(contacts);
        return new ResponseEntity<>(contacts, HttpStatus.OK);
    }
}
