package com.alesharik.talantfinder.rest;

import com.alesharik.talantfinder.database.repository.HumanRepository;
import com.alesharik.talantfinder.rest.exception.UnauthorizedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class LoginController {
    @Autowired
    private HumanRepository repository;
    @Autowired
    private AuthenticationManager authenticationManager;

    @GetMapping("/user/login")
    public void login(@RequestParam("login") String login, @RequestParam("password") String password) {
        UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(login, password);
        Authentication authentication = authenticationManager.authenticate(token);
        if(!authentication.isAuthenticated())
            throw new UnauthorizedException();
        SecurityContextHolder.getContext().setAuthentication(authentication);
    }
}
