package com.alesharik.talantfinder.rest;

import com.alesharik.talantfinder.database.entity.Ability;
import com.alesharik.talantfinder.database.entity.AbilityType;
import com.alesharik.talantfinder.database.entity.Human;
import com.alesharik.talantfinder.database.repository.AbilityRepository;
import com.alesharik.talantfinder.database.repository.AbilityTypeRepository;
import com.alesharik.talantfinder.database.repository.HumanRepository;
import com.alesharik.talantfinder.rest.exception.NotFoundException;
import com.alesharik.talantfinder.rest.security.UserDetailsImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/user")
public class AbilityController {
    @Autowired
    private HumanRepository humanRepository;
    @Autowired
    private AbilityRepository abilityRepository;
    @Autowired
    private AbilityTypeRepository abilityTypeRepository;

    @GetMapping("/abilities")
    public ResponseEntity<Object> get() {
        long id = ((UserDetailsImpl) SecurityContextHolder.getContext().getAuthentication().getDetails()).getId();
        Human human = humanRepository.findById(id).orElseThrow(NotFoundException::new);
        return new ResponseEntity<>(human.getAbilities(), HttpStatus.OK);
    }

    @PutMapping("/ability")
    public ResponseEntity<Object> put(@RequestParam("name") String name, @RequestParam("type") long type) {
        AbilityType abilityType = abilityTypeRepository.findById(type).orElseThrow(NotFoundException::new);
        Human human = ((UserDetailsImpl) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getHuman();
        Ability ability = new Ability(name, abilityType);
        Ability save = abilityRepository.save(ability);
        human.getAbilities().add(save);
        humanRepository.save(human);
        return new ResponseEntity<>(ability, HttpStatus.OK);
    }

    @DeleteMapping("/ability")
    public ResponseEntity<Object> delete(@RequestParam("id") long id) {
        Human human = ((UserDetailsImpl) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getHuman();
        Ability ability = abilityRepository.findById(id).orElseThrow(NotFoundException::new);
        human.getAbilities().remove(ability);
        humanRepository.save(human);
        return new ResponseEntity<>(ability, HttpStatus.OK);
    }
}
