package com.alesharik.talantfinder.rest;

import com.alesharik.talantfinder.database.entity.Human;
import com.alesharik.talantfinder.database.repository.HumanRepository;
import com.alesharik.talantfinder.rest.security.Security;
import com.alesharik.talantfinder.rest.security.UserDetailsImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/user")
public class UserDataController {
    @Autowired
    private HumanRepository repository;

    @GetMapping("/data")
    public ResponseEntity<Human> get() {
        Human human = ((UserDetailsImpl) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getHuman();
        return new ResponseEntity<>(human, HttpStatus.OK);
    }

    @PutMapping("/data")
    public void patchUserData(@RequestParam(required = false, value = "login") String login, @RequestParam(required = false, value = "password") String password, @RequestBody(required = false) byte[] body) {
        Human human = ((UserDetailsImpl) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getHuman();
        if(login != null)
            human.setLogin(login);
        if(password != null)
            human.setPassword(Security.encodePassword(password));
        if(body != null)
            human.setAvatar(body);
        repository.save(human);
    }
}
