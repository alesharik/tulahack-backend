package com.alesharik.talantfinder.rest;

import com.alesharik.talantfinder.database.entity.AbilityType;
import com.alesharik.talantfinder.database.repository.AbilityTypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/admin/ability")
public class AbilityTypeController {
    @Autowired
    private AbilityTypeRepository abilityTypeRepository;

    @PutMapping("/type")
    public ResponseEntity<Object> putType(@RequestParam("name") String name, @RequestParam("color") String color) {
        AbilityType type = new AbilityType(name, color);
        abilityTypeRepository.save(type);
        return new ResponseEntity<>(type, HttpStatus.OK);
    }
}
