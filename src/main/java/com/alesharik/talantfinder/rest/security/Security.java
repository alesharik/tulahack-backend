package com.alesharik.talantfinder.rest.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
@EnableWebSecurity
public class Security extends WebSecurityConfigurerAdapter {
    public static final BCryptPasswordEncoder PASSWORD_ENCODER = new BCryptPasswordEncoder();
    @Autowired
    private UserDetailsService service;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        try {
            http
                    .csrf().disable()
                    .authorizeRequests()
                    .antMatchers("/user/login").permitAll()
                    .antMatchers("/success").permitAll()
                    .anyRequest().authenticated()
                    .and()
                    .formLogin()
                    .loginProcessingUrl("/user/login")
                    .usernameParameter("login")
                    .passwordParameter("password")
                    .defaultSuccessUrl("/success")
                    .permitAll()
                    .and();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Autowired
    public void configAuth(AuthenticationManagerBuilder builder) {
        try {
            builder.userDetailsService(service);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Bean("AuthenticationManager")
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Bean
    public PasswordEncoder passwordEncoderBean() {
        return PASSWORD_ENCODER;
    }

    public static String encodePassword(String password) {
        return PASSWORD_ENCODER.encode(password);
    }
}
