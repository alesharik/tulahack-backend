package com.alesharik.talantfinder.database.repository;

import com.alesharik.talantfinder.database.entity.Ability;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AbilityRepository extends CrudRepository<Ability, Long> {
}
