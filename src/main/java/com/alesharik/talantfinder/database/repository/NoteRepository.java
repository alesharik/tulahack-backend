package com.alesharik.talantfinder.database.repository;

import com.alesharik.talantfinder.database.entity.Note;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface NoteRepository extends CrudRepository<Note, Long> {
}
