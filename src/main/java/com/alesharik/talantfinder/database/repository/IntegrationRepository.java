package com.alesharik.talantfinder.database.repository;

import com.alesharik.talantfinder.database.entity.Integration;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IntegrationRepository extends CrudRepository<Integration, Long> {
}
