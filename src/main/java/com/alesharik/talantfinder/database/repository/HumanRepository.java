package com.alesharik.talantfinder.database.repository;

import com.alesharik.talantfinder.database.entity.Human;
import com.alesharik.talantfinder.rest.security.Security;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.Optional;

@Repository
public interface HumanRepository extends CrudRepository<Human, Long> {
    @Query("SELECT user from Human user WHERE user.role = :role")
    Collection<Human> findAllByRole(@Param("role") Human.Role role);

    @Query("SELECT user from Human user WHERE user.login = :login")
    Optional<Human> findFirstByLogin(@Param("login") String login);

    default Human createUser(Human.Role role, String name, String login, String password) {
        Human human = new Human(role, name, login, Security.encodePassword(password));
        return save(human);
    }
}
