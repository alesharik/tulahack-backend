package com.alesharik.talantfinder.database.repository;

import com.alesharik.talantfinder.database.entity.Stats;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StatsRepository extends CrudRepository<Stats, Long> {
}
