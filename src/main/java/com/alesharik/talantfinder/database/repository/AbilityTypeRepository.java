package com.alesharik.talantfinder.database.repository;

import com.alesharik.talantfinder.database.entity.AbilityType;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AbilityTypeRepository extends CrudRepository<AbilityType, Long> {
}
