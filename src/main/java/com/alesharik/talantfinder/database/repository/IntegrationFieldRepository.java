package com.alesharik.talantfinder.database.repository;

import com.alesharik.talantfinder.database.entity.IntegrationField;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IntegrationFieldRepository extends CrudRepository<IntegrationField, Long> {
}
