package com.alesharik.talantfinder.database.repository;

import com.alesharik.talantfinder.database.entity.Contacts;
import org.springframework.data.repository.CrudRepository;

public interface ContactsRepository extends CrudRepository<Contacts, Long> {
}
