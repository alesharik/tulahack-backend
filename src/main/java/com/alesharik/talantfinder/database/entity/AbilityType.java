package com.alesharik.talantfinder.database.entity;

import lombok.Data;
import org.springframework.lang.NonNull;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
@Data
public class AbilityType {
    @GeneratedValue
    @Id
    private long id;
    @Column(length = 128)
    @NonNull
    private String name;
    @Column(length = 7)
    @NonNull
    private String color;

    public AbilityType() {
        name = "None";
        color = "None";
    }

    public AbilityType(String name, String color) {
        this.name = name;
        this.color = color;
    }
}
