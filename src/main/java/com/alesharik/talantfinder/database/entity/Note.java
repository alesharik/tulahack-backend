package com.alesharik.talantfinder.database.entity;

import lombok.Data;
import lombok.NonNull;

import javax.persistence.*;

@Entity
@Data
public class Note {
    @GeneratedValue
    @Id
    private long id;

    @NonNull
    private Note.NoteValue note;
    @NonNull
    @ManyToOne(fetch = FetchType.EAGER)
    private Human setter;

    public Note() {
        note = null;
        setter = null;
    }

    public Note(NoteValue note, Human setter) {
        this.note = note;
        this.setter = setter;
    }

    public enum NoteValue {
        NONE,
        ONE,
        TWO,
        THREE,
        FOUR,
        FIVE;

        public int toInt() {
            switch (this) {
                case ONE:
                    return 1;
                case TWO:
                    return 2;
                case THREE:
                    return 3;
                case FOUR:
                    return 4;
                case FIVE:
                    return 5;
                default:
                    throw new IllegalStateException("Cannot get int value from " + this);
            }
        }

        public static NoteValue fromInt(int i) {
            if(i == 0)
                return NONE;
            else if(i == 1)
                return ONE;
            else if(i == 2)
                return TWO;
            else if(i == 3)
                return THREE;
            else if(i == 4)
                return FOUR;
            else if(i == 5)
                return FIVE;
            throw new IllegalStateException("OOPS " + i);
        }
    }
}
