package com.alesharik.talantfinder.database.entity;

import lombok.Data;
import org.springframework.lang.NonNull;

import javax.persistence.*;

@Entity
@Data
public class Ability {
    @GeneratedValue
    @Id
    private long id;
    @Column(length = 128)
    @NonNull
    private String name;
    @ManyToOne(fetch = FetchType.EAGER)
    @NonNull
    private AbilityType type;

    public Ability() {
        name = "None";
        type = null;
    }

    public Ability(String name, AbilityType type) {
        this.name = name;
        this.type = type;
    }
}
