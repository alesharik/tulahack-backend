package com.alesharik.talantfinder.database.entity;

import lombok.Data;

import javax.persistence.*;
import java.util.Set;

@Entity
@Data
public class Integration {
    @Id
    @GeneratedValue
    private long id;

    private long integrationUid;
    @OneToMany(fetch = FetchType.EAGER)
    private Set<IntegrationField> fields;

    public Integration() {
        integrationUid = -1;
    }
}
