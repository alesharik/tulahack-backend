package com.alesharik.talantfinder.database.entity;

import lombok.Data;
import org.springframework.lang.NonNull;

import javax.persistence.*;
import java.util.Set;

@Entity
@Data
public class Stats {
    @GeneratedValue
    @Id
    private long id;

    @ManyToOne(fetch = FetchType.EAGER)
    @NonNull
    private Ability ability;

    private int hours;
    @OneToMany(fetch = FetchType.EAGER)
    private Set<Note> notes;

    public Stats() {
        ability = null;
        hours = 0;
    }

    public Stats(Ability ability, int hours, Set<Note> notes) {
        this.ability = ability;
        this.hours = hours;
        this.notes = notes;
    }
}
