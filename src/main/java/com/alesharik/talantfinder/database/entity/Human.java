package com.alesharik.talantfinder.database.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import org.springframework.lang.NonNull;

import javax.persistence.*;
import java.util.Set;

@Entity
@Data
public class Human {
    @Id
    @GeneratedValue
    private long id;
    @NonNull
    private Role role;

    @Column(length = 500)
    @NonNull
    private String name;
    private byte[] avatar;

    @Column(length = 64)
    @NonNull
    private String login;
    @NonNull
    @JsonIgnore
    private String password;

    @ManyToMany(fetch = FetchType.EAGER)
    private Set<Ability> abilities;
    @OneToMany(fetch = FetchType.EAGER)
    private Set<Stats> stats;
    @OneToMany(fetch = FetchType.EAGER)
    private Set<Integration> integrations;
    @OneToMany(fetch = FetchType.EAGER)
    private Set<Contacts> contacts;

    public Human() {
        role = Role.USER;
        name = "";
        login = "";
        password = "";
    }

    public Human(Role role, String name, String login, String password) {
        this.role = role;
        this.name = name;
        this.login = login;
        this.password = password;
    }

    public enum Role {
        USER,
        HUMAN_RESOURCE,
        ADMIN
    }
}
