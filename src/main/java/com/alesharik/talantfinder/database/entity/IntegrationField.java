package com.alesharik.talantfinder.database.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
@Data
public class IntegrationField {
    @Id
    @GeneratedValue
    private long id;
    @Column(length = 80)
    private String key;
    private String value;

    public IntegrationField() {
        key = "";
        value = "";
    }

    public IntegrationField(String key, String value) {
        this.key = key;
        this.value = value;
    }
}
