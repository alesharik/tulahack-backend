package com.alesharik.talantfinder.database.entity;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
public class Contacts {
    @Id
    @GeneratedValue
    private long id;
    @ManyToOne
    private Human human;

    private String service;
    private String address;

    public Contacts() {
        service = "";
        address = "";
    }

    public Contacts(String service, String address) {
        this.service = service;
        this.address = address;
    }
}
