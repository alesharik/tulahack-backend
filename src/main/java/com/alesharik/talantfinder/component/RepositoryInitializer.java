package com.alesharik.talantfinder.component;

import com.alesharik.talantfinder.database.repository.*;
import com.alesharik.talantfinder.util.InitialRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@Slf4j
public class RepositoryInitializer {

    @Bean
    public CommandLineRunner databaseSetup(HumanRepository repository, AbilityTypeRepository abilityTypeRepository, AbilityRepository abilityRepository, ContactsRepository contactsRepository, NoteRepository noteRepository, StatsRepository statsRepository) {
        return args -> {
            log.info("Checking repository");
            if(InitialRepository.isNotInitialized(repository)) {
                log.warn("Initializing repository");
                InitialRepository.initRepository(repository);
            } else
                log.info("Repository already initialized");
//            InitialRepository.initRandomData(abilityTypeRepository, abilityRepository, contactsRepository, repository, noteRepository, statsRepository);
        };
    }
}
