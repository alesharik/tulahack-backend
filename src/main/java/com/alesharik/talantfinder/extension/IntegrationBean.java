package com.alesharik.talantfinder.extension;

public interface IntegrationBean {
    long getUid();

    void init();

    void shutdown();

    SearchQueryManager getSearchQueryManager();
}
