package com.alesharik.talantfinder.extension;

import com.alesharik.talantfinder.database.entity.Human;

import java.util.Map;

public interface SearchQueryManager {
    boolean isValid(Map<String, String> inExtensions, Human human);
}
