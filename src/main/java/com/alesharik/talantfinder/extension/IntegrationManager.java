package com.alesharik.talantfinder.extension;

import lombok.experimental.UtilityClass;
import org.springframework.context.ConfigurableApplicationContext;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

@UtilityClass
public class IntegrationManager {
    private static final List<IntegrationBean> beans = new CopyOnWriteArrayList<>();

    public static void scanForBeans(ConfigurableApplicationContext context) {
        context.getBeansOfType(IntegrationBean.class).forEach((s, integrationBean) -> beans.add(integrationBean));
    }

    public static List<IntegrationBean> getBeans() {
        return Collections.unmodifiableList(beans);
    }

    public static void start() {
        for (IntegrationBean bean : beans)
            bean.init();
    }

    public static void stop() {
        for (IntegrationBean bean : beans)
            bean.shutdown();
    }
}
